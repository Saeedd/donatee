const express = require("express");
const bodyParser = require("body-parser");
const middlewares = require("./middlewares/middlewares");
const { router } = require("./router");
const { notFoundRouter } = require("./middlewares/404");
  
const app = express();
app.use(bodyParser.json())
middlewares(app);
router(app);

notFoundRouter(app);

exports.runApp = () => {
  app.listen(process.env.APPLICATION_PORT, () => {
    console.log(`application running on ${process.env.APPLICATION_PORT}`);
  });
};
