
const authRoutes = require("./modules/auth/routes");
const userRoutes = require("./modules/user/routes");

  //const { auth } = require("./middlewares/authmiddleware");

exports.router = app => {
  app.use("/api/auth", authRoutes);
  app.use("/api/user", userRoutes);
};


