const express = require("express");
const userController = require("./controller");
const router = express.Router();
router.post("/dashboard", userController.dashboard);
module.exports = router;
