const model = require("./model");
const tokenService = require("./token");



exports.register = async (req, res) => {
      console.log('controllereee')
  const { phone_number , email , password } = req.body;

  if (!phone_number || !email || !password) {
    res.status(422).send({
      success: false,
      message: "اطلاعات مورد نیاز برای عضویت را تکمیل کنید"
    });
  }

  const userData = {
    phone_number,
    email,
    password
  };
  const checkExistUser = await model.findUserByEmail(email);
  if  (checkExistUser) {
     return res.send({
      success: false,
      message: "کاربر با این مشخصات در سیستم ثبت نام کرده است"
       });
     }  
  else{
   const user = await model.createUser(userData);

  if (user.affectedRows > 0) {                      ///////////////////////
    const token = await tokenService.createSign({
      uid: user.insertId,
      username: userData.email
    });

    res.send({
      success: true,
      message: "عضویت با موفقیت انجام شد",
      token
    });
  }
}};




exports.login = async (req, res) => {
  
  const { email , password } = req.body;

  if (!email || !password) {
    res.status(422).send({
      success: false,
      message: "اطلاعات مورد نیاز برای ورود را تکمیل کنید"
    });
  }

  const userLogin = await model.login(email, password);

  if (!userLogin) {
    res.status(401).send({
      success: false,
      message: "کاربری با این مشخصات یافت نشد."
    });
  }

  const token = await tokenService.createSign({
    uid: userLogin.id,
    username: userLogin.username
  });

  res.send({
    success: true,
    message: "ورود با موفقیت انجام شد",
    username: userLogin.phone_number,
    token
  });
};

