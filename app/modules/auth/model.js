const connection = require("../../../database/connection/mysql");



exports.createUser = async user => {
           console.log('modeleeeee')
  const db = await connection();
  const [result, fields] = await db.query("INSERT INTO `user` SET ?", user);
 
 console.log(result);
  return result;  

};
  
  
exports.login = async (email, password) => {
  
  const db = await connection();
  const [result, fields] = await db.query(
    "SELECT * FROM `user` WHERE email = ? AND password = ?",
    [email, password]
  ); 
  if (result.length > 0) {
    return result[0];
  }
  return false;
};





exports.findUserByEmail = async (email) => {
  const db = await connection();
  const [result, fields] = await db.query(
    "SELECT * FROM `user` WHERE email = ?",
    email
  );
  if (result.length > 0) {
    return true;
  }
  return false;
};
