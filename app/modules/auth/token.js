const jwt = require("jsonwebtoken");
exports.createSign = async params => {
  return await jwt.sign(params, process.env.JWT_KEY);
};

exports.verifyToken = async token => {
  try {
    const payload = await jwt.verify(token, process.env.JWT_KEY);
    return payload;
  } catch (error) {
    return false;
  }
};

exports.getToken = async req => {
  if (!("authorization" in req.headers)) {
    return false;
  }

  const { authorization } = req.headers;
  const [prefix, token] = authorization.split(" ");

  if (!token) {
    return false;
  }
  return token;
};
