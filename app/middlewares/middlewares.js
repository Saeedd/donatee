const bodyParser = require("body-parser");
const session = require("express-session");
const cors = require("cors");
    
module.exports = (app) => {
   
  app.use(bodyParser.json())

  console.log('middleeeeeee')
  //app.use(bodyParser.urlencoded({ extended: false }));
   //app.use(bodyParser.json())
  app.use(cors());
  app.use(
    session({
      secret: "keyboard cat",
      resave: false,
      saveUninitialized: true,
      cookie: { secure: false }
    })
  );
};
