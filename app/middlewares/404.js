exports.notFoundRouter = app => {
  app.use((req, res) => {
    return res.status(404).send({
      message: "متاسفانه صفحه ای یافت نشد."
    });
  });
};
