require("dotenv").config();

const mysql = require("mysql2/promise");
const connection = async () => {
  const db = await mysql.createConnection({
    host: process.env.HOST_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
  });
  return db;
};
module.exports = connection;
